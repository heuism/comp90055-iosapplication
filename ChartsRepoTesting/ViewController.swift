//
//  ViewController.swift
//  ChartsRepoTesting
//
//  Created by Hien Tran on 3/1/18.
//  Copyright © 2018 Awesome Team. All rights reserved.
//

import UIKit
import Charts
import SwiftSocket

// Put this at file level anywhere in your project
precedencegroup PowerPrecedence { higherThan: MultiplicationPrecedence }

infix operator ^^ : PowerPrecedence
func ^^ (radix: Int, power: Int) -> Int {
    return Int(pow(Double(radix), Double(power)))
}

class ViewController: UIViewController {
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var enterBtn: UIButton!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var connectionBtn: UIButton!
    @IBOutlet weak var repCntLbl: UILabel!
    
    @IBOutlet weak var patternChartView: LineChartView!
    
    var connected: Bool = false
    var LastPos = 0
    var dataArray = SynchronizedArray<Double>()
    var processedDataArray = SynchronizedArray<Double>()
    var processedDataArrayDictionary = Dictionary<Int, Dictionary<String, Any>>()
    var patternArray = SynchronizedArray<Double>()
    var clientCpp: TCPClient?
    var clientPython: TCPClient?
    
//    let MAX_NUMBER = 1000
    let MAX_NUMBER = 6000
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        update1Mil()
//        updateGraph(graph: lineChartView, data: dataArray, labelName: "Number", scaleEn: true, lineColors: [UIColor.blue])
//        updateGraph(graph: patternChartView, data: dataArray, labelName: "Pattern", scaleEn: false, lineColors: [UIColor.yellow, UIColor.red])
        connected = false
        updateBtnInfo(btn: connectionBtn, cn: connected)
    }
    
    func flipVal(val: Bool) -> Bool {
        return !val
    }
    
    func fromStringArrToDoubleArr(stringArr: [String], oldAngle: Double) -> [Double] {
        var doubleArr = [Double]()
        for str in stringArr {
            if let val = Double(str) {
                doubleArr.append(val)
            }
            else {
                doubleArr.append(oldAngle)
            }
        }
        return doubleArr
    }
    
    func processAngle(oldAngle: Double, data: [Double], pos: Int, dt: Double) -> Double {
        var newAngle = Double()
        newAngle = 0.98*(oldAngle + data[pos-1+6]*dt) + 0.02*(data[pos-1])
        return newAngle
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("DID DISAPPEARED!!!")
        clientCpp?.close()
        clientPython?.close()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateBtnInfo(btn: UIButton, cn: Bool) {
        if cn {
            btn.backgroundColor = #colorLiteral(red: 0.7774708867, green: 0.07326642424, blue: 0.04332686216, alpha: 1)
            btn.setTitle("Disconnect", for: .normal)
            btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            return
        }
        
        btn.backgroundColor = #colorLiteral(red: 0.4274509804, green: 0.737254902, blue: 0.3882352941, alpha: 1)
        btn.setTitle("Connect", for: .normal)
        btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        return
    }
    
    func disConnectUpdate(client: TCPClient?) {
        if self.connected {
            self.connected = self.flipVal(val: self.connected)
            self.updateBtnInfo(btn: self.connectionBtn, cn: self.connected)
            client?.close()
        }
    }
    
    func openDispatch(client: TCPClient?, lineChart: LineChartView, angleFunctor: @escaping (_ angles: [Double])->Void) {
        print("STARTING DISPATCH")
        var queue = DispatchQueue.global(qos: .background).async(flags: .barrier) {
            //            let client = TCPClient(address: "192.168.0.4", port: 8888)
            switch client?.connect(timeout: 1) {
            case .success?:
                // Connection successful 🎉
                print(client?.port)
                print("🎉")
            case .failure(let error)?:
                // 💩
                print("OH 💩 -> \(error)")
                DispatchQueue.main.async {
                    self.disConnectUpdate(client: client)
                }
                return
            case .none:
                print("OH WELL")
                return
            }
//            guard let data = client?.read(1024*10) else {return}
//                        print("Connect Data is: \(data)")
//            if let response = String(bytes: data, encoding: .utf8) {
//                print("The response is: \(response)")
//                let arrays = response.split(separator: "\n")
//                for arr in arrays {
//                    let numbers = arr.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: ",")
//                    print(numbers)
//                }
//            }
            //            var data = client.read(1024*10) //return optional [Int8]
            //            print("Data is \(data)")
            var working: Bool = true
            var oldAngle: Double = 0.0
            //            self.updateAngleFromSensor(angle: oldAngle)
            while working {
                if let new = client?.read(1024*10, timeout: 40) {
                    //                    print("New data is: \(new)")
                    if let response = String(bytes: new, encoding: .utf8) {
                        let arrays = response.split(separator: "\n")
                        for arr in arrays {
                            print(arr)
                            let numbers = arr.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: ",")
                            print(numbers)
                            let doubleArr = self.fromStringArrToDoubleArr(stringArr: numbers, oldAngle: oldAngle)
                            let newAngle = doubleArr[0]
//                            angleFunctor(newAngle)
                            angleFunctor(doubleArr)
//                            self.updateAngleFromSensor(angle: newAngle)
                            DispatchQueue.main.async {
                                self.synchroniseDataToGraph()
                            }
                            oldAngle = newAngle
                        }
                    }
                } else {
                    print("Can't READ data")
                    DispatchQueue.main.async {
                        self.disConnectUpdate(client: client)
                    }
                    return
                }
                //                data = client.read(1024*10)
                //                print("New data is: \(data)")
            }
        }
    }

    @IBAction func btnPressed(_ sender: Any) {
        if connected {
            connected = flipVal(val: connected)
            clientCpp?.close()
            clientPython?.close()
            updateBtnInfo(btn: connectionBtn, cn: connected)
            return
        }
        
        if let textVal = numberTextField.text {
            connected = flipVal(val: connected)
            updateBtnInfo(btn: connectionBtn, cn: connected)
            clientCpp?.close()
            clientPython?.close()
            clientCpp = TCPClient(address: textVal, port: 8888)
            clientPython = TCPClient(address: textVal, port: 9999)
            openDispatch(client: clientCpp, lineChart: lineChartView, angleFunctor: updateAngleFromSensor(_:))
            openDispatch(client: clientPython, lineChart: lineChartView, angleFunctor: updateProcessedAngleFromSensor(_:))
        }
    }
    
    func synchroniseDataToGraph() {
        updateGraph(graph: lineChartView, data: dataArray, processedData: processedDataArray, labelName: "Number", labelProcessName: "Processed", scaleEn: false, lineColors: [NSUIColor.blue], lineProcessedColors: [NSUIColor.yellow], lineProcessedColorsWrong: [NSUIColor.red])
    }
    
//    func updateGraph(graph: LineChartView, data: [Double], processedData: [Double], labelName: String, labelProcessName: String, scaleEn: Bool, lineColors: [UIColor], lineProcessedColors: [UIColor]) {
    func updateGraph(graph: LineChartView, data: SynchronizedArray<Double>, processedData: SynchronizedArray<Double>, labelName: String, labelProcessName: String, scaleEn: Bool, lineColors: [UIColor], lineProcessedColors: [UIColor], lineProcessedColorsWrong: [UIColor]) {
        var lineChartEntry = [ChartDataEntry]()
        objc_sync_enter(data)
        let dataCount = data.count
        print("Data Count: \(dataCount)")
        for i in 0..<dataCount {
            let value = ChartDataEntry(x: Double(i), y: data[i]!)
//            print("Data at \(i) is: \(data[i])")
            lineChartEntry.append(value)
        }
        objc_sync_exit(data)
        var lineChartDataSetArr = [LineChartDataSet]()
        
        for (key, dictionary) in processedDataArrayDictionary {
            var lineProcessedChartEntry = [ChartDataEntry]()
            print("start position: \(key)")
            let tmpArr = dictionary["dataArray"] as! SynchronizedArray<Double>
            let tmpCorrect = dictionary["correct"] as! Int
            for i in 0..<tmpArr.count {
                if let val = tmpArr[i]{
                    let value = ChartDataEntry(x: Double(i), y: val)
                    lineProcessedChartEntry.append(value)
                } else {
                    break
                }
            }
            var line = LineChartDataSet(values: lineProcessedChartEntry, label: labelProcessName)
            if (tmpCorrect == 0) {
                line.colors = lineProcessedColorsWrong
            } else {
                line.colors = lineProcessedColors
            }
            line.drawCirclesEnabled = false
            lineChartDataSetArr.append(line)
        }
        
        let line1 = LineChartDataSet(values: lineChartEntry, label: labelName)
        line1.colors = lineColors
        
        line1.drawCirclesEnabled = false
        
        line1.drawValuesEnabled = false
        
        let data = LineChartData()
        data.addDataSet(line1)
        
        for l in lineChartDataSetArr {
            data.addDataSet(l)
        }
        
        graph.data = data
        graph.setVisibleXRangeMaximum(1000)
        graph.setScaleEnabled(scaleEn)
        graph.chartDescription?.text = "My Angle Data Chart"
    }
    
    func update1Mil() {
        for i in 0...10 {
            dataArray.append(Double(i))
            patternArray.append(Double(i))
        }
    }
    
    func updateAngleFromSensor(_ angles: [Double]) {
        objc_sync_enter(dataArray)
        objc_sync_enter(processedDataArray)
        let angle = angles[0]
        print("NORMAL ANGLE")
        LastPos = Int(angles.last!)
        dataArray.append(angle)
        while dataArray.count > MAX_NUMBER {
            dataArray.remove(at: 0)
            let keys = Array(processedDataArrayDictionary.keys).sorted()
            if let first = keys.first {
                var dict = processedDataArrayDictionary[first] as! Dictionary<String, Any>
                var dataArr = dict["dataArray"] as! SynchronizedArray<Double>
                dataArr.remove(at: 0)
                if dataArr.count == 0 {
                    processedDataArrayDictionary.removeValue(forKey: first)
                } else {
                    dict.updateValue(dataArr, forKey: "dataArray")
                    processedDataArrayDictionary.updateValue(dict, forKey: first)
                }
            }
        }
        objc_sync_exit(dataArray)
        objc_sync_exit(processedDataArray)
    }
    
    func updateProcessedAngleFromSensor(_ angles: [Double]) {
        objc_sync_enter(dataArray)
        objc_sync_enter(processedDataArray)
        print("The array receive is: \(angles) -> PROCESS ANGLE")
        let start = Int(angles.first!)
        let end = Int(angles[1])
        let correct = Int(angles.last!)
        if dataArray.count >= (start - end) {
            
        }
        processedDataArray = SynchronizedArray<Double>()
//        for i in 0..<dataArray.count-(LastPos - end) {
//            processedDataArray.append(dataArray[i]!)
//        }
        let dataCount = dataArray.count
        var lowestPos = 0
        
        if LastPos - start >= dataCount - 1 {
            lowestPos = 0
        } else {
            lowestPos = dataCount - (LastPos - start) - 1
        }
        
        for i in lowestPos..<dataCount-(LastPos - end) {
            processedDataArray.append(dataArray[i]!)
        }
        print(processedDataArray)
        var newDictionary = Dictionary<String, Any>()
        newDictionary["dataArray"] = processedDataArray
        newDictionary["correct"] = correct
        processedDataArrayDictionary[start] = newDictionary
        objc_sync_exit(dataArray)
        objc_sync_exit(processedDataArray)
//        print("PROCESS ANGLE")
//        processedDataArray.append(angle)
//        if processedDataArray.count > MAX_NUMBER {
//            processedDataArray.remove(at: 0)
//        }
    }
}

